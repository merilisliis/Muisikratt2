var allCollections;
// Nüüd teeme nn. AJAX-päringu - küsime JavaScriptiga JSON-i käest andmeid:
// NB! See kasutab CollectionController.java koodis loodud JSON-i-meetodi sisu!
function getAllCollections() {
	$.getJSON("http://localhost:8080/muisikratt2/rest/collections",
			function(collections) {
				allCollections = collections;
				console.log(collections);
				var optionBody = document.getElementById("collectionOptionBody");
				var collectionSelectOptions = "<option value= '0'>*** Kõik kogud ***</option>";
				for (var i = 0; i < collections.length; i++) {
					collectionSelectOptions = collectionSelectOptions +
					"<option value='" + collections[i].id + "'>" 
					+ collections[i].collectionName + "</option>";
				
				} //Siin üleval on tavaline string, millele ma panen muutujate väärtused sisse (sellega teen ühtlasi nupu, mis juhib märkuse lisamise võimaluse juurde)
				optionBody.innerHTML = collectionSelectOptions;
			});
}
getAllCollections();

function checkIfNullOrUndefined(value) {
	if (value != 0 && value != undefined && value != "null") {
		return value;
	} else {
		return "-";
	}
}