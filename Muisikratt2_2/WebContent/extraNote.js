var dataTableExtraNotes;
var extraNotesTable;
showHideElementById("addNewExtraNoteModal");

function fillExtraNotesTable(extraNotes) {
	if (dataTableExtraNotes != undefined) {
		dataTableExtraNotes.clear();
		dataTableExtraNotes.destroy();
	}
	// Teeme funktsiooni kuupäeva näitamise viisi teisendamiseks:
	var tableBody = document.getElementById("extraNotesTableBody");
	var tableContent = "";
	for (var i = 0; i < extraNotes.length; i++) {
		var d = extraNotes[i].date;
		var datestr = d.slice(0, 10).split("-");
		var dateString = datestr[2] + "." + datestr[1] + "." + datestr[0];
		tableContent = tableContent + "<tr><td>" + dateString + "</td><td>"
				+ extraNotes[i].clientName + "</td><td>"
				+ extraNotes[i].extraNoteText
		"</td></tr>";
	}
	
	tableBody.innerHTML = tableContent;
	dataTableExtraNotes = $('#extraNotesTable').DataTable({
		"language" : {
			"url" : "//cdn.datatables.net/plug-ins/1.10.16/i18n/Estonian.json"
		}
	});
}

function checkIfNullOrUndefined(value) {
	if (value != 0 && value != undefined && value != "null") {
		return value;
	} else {
		return "-";
	}
}

// Funktsioon extranote´ide välja valimiseks MuisLinki järgi:
function getExtraNotesByMuisLink(muisLink) {
	if (muisLink != "") {
		$.getJSON(
				"http://localhost:8080/muisikratt2/rest/extra_notes/by_muis_link?muisLink="
						+ muisLink, function(extraNotes) {
					allExtraNotes = extraNotes;
					fillExtraNotesTable(extraNotes)
				});
	}
}

function showAddNewExtraNoteModal(muisLink) {
	document.getElementById("musealLink").setAttribute("href", muisLink);
	document.getElementById("muisLink").value = muisLink;
	clearAddExtraNoteModalFields();
}

function clearAddExtraNoteModalFields() {
	document.getElementById("eMail").value = "";
	document.getElementById("clientName").value = "";
	document.getElementById("extraNoteText").value = "";
}

function showExtraNotes(muisLink) {
	document.getElementById("musealLinkforExtraNotes").setAttribute("href",
			muisLink);
	getExtraNotesByMuisLink(muisLink);
}

// E-posti välja täidetud olemise kontoll:
function validateEmailInExtraNotes() {
	if (document.getElementById("eMail").value == ""
			|| document.getElementById("eMail").value == null) {
		alert("Palun sisesta e-posti aadress!");
		return false;
	} else {
		return true;
	}
}

function addNewExtraNote() {
	if (validateEmailInExtraNotes()) {
		var muisLinkString = muisLink.toString();
		var newExtraNoteJson = {
			"EMail" : document.getElementById("eMail").value,
			"clientName" : document.getElementById("clientName").value,
			"extraNoteText" : document.getElementById("extraNoteText").value,
			"muisLink" : document.getElementById("muisLink").value
		}

		console.log(newExtraNoteJson);
		var newExtraNote = JSON.stringify(newExtraNoteJson);

		$.ajax({
			url : "http://localhost:8080/muisikratt2/rest/extra_notes",
			type : "POST",
			data : newExtraNote,
			contentType : "application/json; charset=utf-8",
			success : function() {
				$("#addNewExtraNoteModal").modal("hide");
				getExtraNotesByMuisLink();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log(textStatus);
			}
		});
	}
}