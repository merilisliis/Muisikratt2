var allItems;
var dataTable;
//Siin on muutuja, mille väärtust muudetakse päringuvastuste tabeli laadimisega, et getAllItems-meetod saaks käivituda. 
var initialLoadDoneForSelects = false;
// Nüüd teeme nn. AJAX-päringu - küsime JavaScriptiga JSON-i käest andmeid:
// NB! See kasutab AdressController.java koodis loodud JSON-i-meetodi sisu!
function getAllItems() {
	$
			.getJSON(
					"http://localhost:8080/muisikratt2/rest/items",
					function(items) {
						allItems = items;
						var tableBody = document
								.getElementById("itemTableBody");
						var tableContent = "";
						for (var i = 0; i < items.length; i++) {
							tableContent = tableContent
									+ "<tr><td>"
									+ "<a href='" + items[i].muisLink
									+ "' target='_blank' class='button'>Link</a></td><td>"
									+ items[i].identifier
									+ "</td><td>"
									+ checkIfNullOrUndefined(items[i].essence)
									+ "</td><td>"
									+ items[i].title
									+ "</td><td>"
									+ checkIfNullOrUndefined(items[i].person)
									+ "</td><td>"
									+ checkIfNullOrUndefined(items[i].group)
									+ "</td><td>"
									+ checkIfNullOrUndefined(items[i].dcTermsDate)
									+ "</td><td>"
									+ checkIfNullOrUndefined(items[i].tookPlaceAt)
									+ "</td><td>"
									+ checkIfNullOrUndefined(checkIfNullOrUndefined(items[i].part).partName)
									+ "</td><td>"
									+ checkIfNullOrUndefined(items[i].description)
									+ "</td><td>"
									+ checkIfNullOrUndefined(items[i].collection.collectionName)
									+ "</td><td><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#showExtraNotesViewingModal' onClick=\"showExtraNotes('"
									+ items[i].muisLink
									+ "')\" "
									+ ((items[i].numberOfNotes > 0) ? ""
											: "disabled")
									+ " >Märkuste vaatamine("
									+ items[i].numberOfNotes
									+ ")</button>"
									+ "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#addNewExtraNoteModal' onClick=\"showAddNewExtraNoteModal('"
									+ items[i].muisLink
									+ "')\">Märkuse lisamine</button></td></tr>";
						} // Siin üleval on tavaline string, millele ma panen
						// muutujate väärtused sisse (sellega teen ühtlasi
						// nupu, mis juhib märkuse lisamise võimaluse
						// juurde)
						tableBody.innerHTML = tableContent;
						dataTable = $('#muisDataTable')
								.DataTable(
										{
											"language" : {
												"url" : "//cdn.datatables.net/plug-ins/1.10.16/i18n/Estonian.json"
											}
										});
					});
}
getAllItems();

function checkIfNullOrUndefined(value) {
	if (value != 0 && value != undefined && value != "null") {
		return value;
	} else {
		return "-";
	}
}

function showMuisLink() {
document.getElementById("musealLinkforExtraNotes").setAttribute("href",
		muisLink);
}

function getItemsByCollectionIdAndPartId() {
	if (dataTable != undefined) {
		dataTable.clear();
		dataTable.destroy();
	}

	var selectedCollectionValue = document
			.getElementById("collectionOptionBody").value;
	var selectedPartValue = document.getElementById("partOptionBody").value;
	var a;
	// "value" keyword tähistab mistahes väärtust, mis siin id-ks olla võiks.
	if (selectedCollectionValue == 0 && initialLoadDoneForSelects
			&& selectedPartValue == 0 ) {
		getAllItems()
	} else {
		if (selectedCollectionValue != 0 && selectedPartValue == 0) {
			a = "http://localhost:8080/muisikratt2/rest/items/bycollection/"
					+ selectedCollectionValue
		} else if (selectedPartValue != 0 && selectedCollectionValue == 0) {
			a = "http://localhost:8080/muisikratt2/rest/items/bypart/"
					+ selectedPartValue
		} else {
			a = "http://localhost:8080/muisikratt2/rest/items/collections/"
					+ selectedCollectionValue + "/parts/" + selectedPartValue
		}
		
		$
				.getJSON(
						a,
						function(items) {
							allItems = items;
							console.log(items);
							var tableBody = document
									.getElementById("itemTableBody");
							var tableContent = "";
							for (var i = 0; i < items.length; i++) {
								tableContent = tableContent
								+ "<tr><td>"
								+ "<a href='" + items[i].muisLink
								+ "' target='_blank' class='button'>Link</a></td><td>"
										+ items[i].identifier
										+ "</td><td>"
										+ checkIfNullOrUndefined(items[i].essence)
										+ "</td><td>"
										+ items[i].title
										+ "</td><td>"
										+ checkIfNullOrUndefined(items[i].person)
										+ "</td><td>"
										+ checkIfNullOrUndefined(items[i].group)
										+ "</td><td>"
										+ checkIfNullOrUndefined(items[i].dcTermsDate)
										+ "</td><td>"
										+ checkIfNullOrUndefined(items[i].tookPlaceAt)
										+ "</td><td>"
										+ checkIfNullOrUndefined(checkIfNullOrUndefined(items[i].part).partName)
										+ "</td><td>"
										+ checkIfNullOrUndefined(items[i].description)
										+ "</td><td>"
										+ checkIfNullOrUndefined(items[i].collection.collectionName)
										+ "</td><td><button type='button' class='btn btn-primary' data-toggle='modal' data-target='#showExtraNotesViewingModal' onClick=\"showExtraNotes('"
										+ items[i].muisLink
										+ "')\" "
										+ ((items[i].numberOfNotes > 0) ? ""
												: "disabled")
										+ " >Märkuste vaatamine("
										+ items[i].numberOfNotes
										+ ")</button>"
										+ "<button type='button' class='btn btn-primary' data-toggle='modal' data-target='#addNewExtraNoteModal' onClick=\"showAddNewExtraNoteModal('"
										+ items[i].muisLink
										+ "')\">Märkuse lisamine</button></td></tr>";
							}
							tableBody.innerHTML = tableContent;
							dataTable = $('#muisDataTable')
									.DataTable(
											{
												"language" : {
													"url" : "//cdn.datatables.net/plug-ins/1.10.16/i18n/Estonian.json"
												}
											});
						});
	}
	initialLoadDoneForSelects = true;
	
}

function checkIfNullOrUndefined(value) {
	if (value != 0 && value != undefined && value != "null") {
		return value;
	} else {
		return "-";
	}
}