var allParts;
// Nüüd teeme nn. AJAX-päringu - küsime JavaScriptiga JSON-i käest andmeid:
// NB! See kasutab CollectionController.java koodis loodud JSON-i-meetodi sisu!
function getAllParts() {
	$.getJSON("http://localhost:8080/muisikratt2/rest/parts",
			function(parts) {
				allParts = parts;
				console.log(parts);
				var optionBody = document.getElementById("partOptionBody");
				var partSelectOptions = "<option value= '0'>*** Kõik kollektsioonid ***</option>";
				for (var i = 0; i < parts.length; i++) {
					partSelectOptions = partSelectOptions +
					"<option value='" + parts[i].partId + "'>" 
					+ parts[i].partName + "</option>";
				} 
				optionBody.innerHTML = partSelectOptions;
			});
}
getAllParts();

function checkIfNullOrUndefined(value) {
	if (value != 0 && value != undefined && value != "null") {
		return value;
	} else {
		return "-";
	}
}