package ee.bcs.koolitus.muisikratt2.controller;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.muisikratt2.source.CollectionResource;
import ee.bcs.koolitus.muisikratt2.dao.Collection;

@Path("/collections")
public class CollectionController {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Collection> getAllCollections() {
		return CollectionResource.getAllCollections();
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection getCollectionsById(@PathParam("id") int id) {
		return CollectionResource.getCollectionById(id);
	}
	
	
	
	
}
