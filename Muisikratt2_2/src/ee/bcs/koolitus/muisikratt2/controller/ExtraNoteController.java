package ee.bcs.koolitus.muisikratt2.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.muisikratt2.source.ExtraNoteResource;
import ee.bcs.koolitus.muisikratt2.dao.ExtraNote;

@Path("/extra_notes")
public class ExtraNoteController {

	@GET
	@Path("/{idExtraNote}")
	@Produces(MediaType.APPLICATION_JSON)
	public ExtraNote getExtraNotesById(@PathParam("idExtraNote") int idExtraNote) {
		return ExtraNoteResource.getExtraNoteById(idExtraNote);
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public ExtraNote addNewExtraNote(ExtraNote extraNote) {
		return ExtraNoteResource.addExtraNote(extraNote);
	}

	@GET
	@Path("/by_muis_link")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ExtraNote> getExtraNotesByMuisLink(@QueryParam("muisLink") String muisLink) {
		return ExtraNoteResource.getExtraNotesByMuisLink(muisLink);
	}
	}