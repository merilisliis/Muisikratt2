package ee.bcs.koolitus.muisikratt2.controller;

import java.util.List;
import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.muisikratt2.source.MuisDataResource;
import ee.bcs.koolitus.muisikratt2.dao.MuisData;

//Siin on tehtud meetod andmete pärimiseks JSON-i kujul andmebaasist
//Siin on meie esimene ressurss - viidatud kujul /items
@Path("/items")
public class MuisDataController {

	@GET
	@Produces(MediaType.APPLICATION_JSON) // See meetod toodab JSON-i
	public List<MuisData> getAllItems() { 
		return MuisDataResource.getAllItems();
	}

	@GET // Siia saab ehitada ka selliseid meetodeid, mis annavad vaid teatud rea sisust
			// sõltuvaid vasteid.
	// Anna neile eri nimed ja tee neid, palju vaja.
	@Path("/{idLocal}") // Siin täiendame URL-i path'i(URL läheb pikemaks) ja pathparameetrid antakse
						// siin selleks, et täpsustada muutujat
	@Produces(MediaType.APPLICATION_JSON)
	public MuisData getItemsById(@PathParam("idLocal") int idLocal) {
		return MuisDataResource.getItemById(idLocal);
	}

	@POST // Siia on ehitatud meetod andmete saatmiseks andmebaasi. tema URL tuleb Path-i
			// meetodist (vt. üleval)
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MuisData addNewExtraNote(MuisData item) {
		return MuisDataResource.addExtraNote(item);
	}

	@GET // Siia saab ehitada ka selliseid meetodeid, mis annavad vaid teatud rea sisust
			// sõltuvaid vasteid.
	@Path("/bycollection/{collectionId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MuisData> getItemsByCollectionId(@PathParam("collectionId") int collectionId) {
		return MuisDataResource.getItemsByCollectionId(collectionId);
	}

	@GET 
	@Path("/bypart/{partId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MuisData> getItemsByPartId(@PathParam("partId") int partId) {
		return MuisDataResource.getItemsByPartId(partId);
	}
	@GET 
	@Path("/collections/{collectionId}/parts/{partId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<MuisData> getItemsByCollectionIdAndPartId(@PathParam("collectionId") int collectionId, @PathParam("partId") int partId) {
		return MuisDataResource.getItemsByCollectionIdAndPartId(collectionId, partId);
	}
	
}