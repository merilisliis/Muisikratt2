package ee.bcs.koolitus.muisikratt2.controller;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.koolitus.muisikratt2.source.PartResource;
import ee.bcs.koolitus.muisikratt2.dao.Part;

@Path("/parts")
public class PartController {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Part> getAllParts() {
		return PartResource.getAllParts();
	}

	@GET
	@Path("/{partId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Part getPartsById(@PathParam("partId") int partId) {
		return PartResource.getPartById(partId);
	}
	
	
	
	
}