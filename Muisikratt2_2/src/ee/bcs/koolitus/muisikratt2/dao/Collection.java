package ee.bcs.koolitus.muisikratt2.dao;

public class Collection {
	private int id;
	private String collectionName;

	public int getId() {
		return id;
	}

	public Collection setId(int id) {
		this.id = id;
		return this;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public Collection setCollectionName(String collectionName) {
		this.collectionName = collectionName;
		return this;
	}

	@Override
	public String toString() {
		return "Collection[id=" + id + "collectionName" + collectionName + "]";

	}
}
