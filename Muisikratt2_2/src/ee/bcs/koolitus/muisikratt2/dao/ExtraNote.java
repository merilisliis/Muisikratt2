package ee.bcs.koolitus.muisikratt2.dao;

import java.sql.Timestamp;

public class ExtraNote {
	private int idExtraNote;
	private String muisLink;
	private Timestamp date;
	private String eMail;
	private String clientName;
	private String extraNoteText;

	public int getIdExtraNote() {
		return idExtraNote;
	}

	public ExtraNote setIdExtraNote(int idExtraNote) {
		this.idExtraNote = idExtraNote;
		return this;
	}

	public String getMuisLink() {
		return muisLink;
	}

	public ExtraNote setMuisLink(String muisLink) {
		this.muisLink = muisLink;
		return this;
	}

	public Timestamp getDate() {
		return date;
	}

	public ExtraNote setDate(Timestamp date) {
		this.date = date;
		return this;
	}

	public String getEMail() {
		return eMail;
	}

	public ExtraNote setEMail(String eMail) {
		this.eMail = eMail;
		return this;
	}

	public String getClientName() {
		return clientName;
	}

	public ExtraNote setClientName(String clientName) {
		this.clientName = clientName;
		return this;
	}

	public String getExtraNoteText() {
		return extraNoteText;
	}

	public ExtraNote setExtraNoteText(String extraNoteText) {
		this.extraNoteText = extraNoteText;
		return this;
	}

	@Override
	public String toString() {
		return "ExtraNote[idExtraNote=" + idExtraNote + "muisLink=" + muisLink + "date=" + date + "eMail=" + eMail
				+ "clientName=" + clientName + "extraNoteText=" + extraNoteText + "]";
	}
}
