package ee.bcs.koolitus.muisikratt2.dao;

public class MuisData {
	private int idLocal;
	private String muisLink;
	private String identifier;
	private String essence;
	private String title;
	private String person;
	private String group;
	private String dcTermsDate;
	private String tookPlaceAt;
	private int partId;
	private Part part; 
	private String description;
	private int collectionId;
	private Collection collection; // luuakse muutuja Collection klassist
	private int idExtraNote;
	private ExtraNote extraNote;
	private int numberOfNotes;

	public int getNumberOfNotes() {
		return numberOfNotes;
	}

	public MuisData setNumberOfNotes(int numberOfNotes) {
		this.numberOfNotes = numberOfNotes;
		return this;
	}

	public int getIdLocal() {
		return idLocal;
	}

	public MuisData setIdLocal(int idLocal) {
		this.idLocal = idLocal;
		return this;
	}

	public String getMuisLink() {
		return muisLink;
	}

	public MuisData setMuisLink(String muisLink) {
		this.muisLink = muisLink;
		return this;
	}

	public String getIdentifier() {
		return identifier;
	}

	public MuisData setIdentifier(String identifier) {
		this.identifier = identifier;
		return this;
	}

	public String getEssence() {
		return essence;
	}

	public MuisData setEssence(String essence) {
		this.essence = essence;
		return this;
	}

	public String getTitle() {
		return title;
	}

	public MuisData setTitle(String title) {
		this.title = title;
		return this;
	}

	public String getPerson() {
		return person;
	}

	public MuisData setPerson(String person) {
		this.person = person;
		return this;
	}

	public String getGroup() {
		return group;
	}

	public MuisData setGroup(String group) {
		this.group = group;
		return this;
	}

	public String getDcTermsDate() {
		return dcTermsDate;
	}

	public MuisData setDcTermsDate(String dcTermsDate) {
		this.dcTermsDate = dcTermsDate;
		return this;
	}

	public String getTookPlaceAt() {
		return tookPlaceAt;
	}

	public MuisData setTookPlaceAt(String tookPlaceAt) {
		this.tookPlaceAt = tookPlaceAt;
		return this;
	}

	public Part getPart() {
		return part;
	}

	public MuisData setPart(Part part) {
		this.part = part;
		return this;
	}
	public int getPartId() {
		return partId;
	}

	public MuisData setPartId(int partId) {
		this.partId = partId;
		return this;
	}

	public String getDescription() {
		return description;
	}


	public MuisData setDescription(String description) {
		this.description = description;
		return this;
	}

	public int getCollectionId() {
		return collectionId;
	}

	public MuisData setCollectionId(int collectionId) {
		this.collectionId = collectionId;
		return this;
	}

	public Collection getCollection() {
		return collection;
	}

	public MuisData setCollection(Collection collection) {
		this.collection = collection;
		return this;
	}

	public int getIdExtraNote() {
		return idExtraNote;
	}

	public MuisData setIdExtraNote(int idExtraNote) {
		this.idExtraNote = idExtraNote;
		return this;
	}

	public ExtraNote getExtraNote() {
		return extraNote;
	}

	public MuisData setExtraNote(ExtraNote extraNote) {
		this.extraNote = extraNote;
		return this;
	}

	@Override
	public String toString() {
		return "MuisData[idLocal=" + idLocal + " " + "muisLink=" + muisLink + " " + "identifier=" + identifier + " "
				+ "essence=" + essence + " " + "title=" + title + " " + "person=" + person + " " + "group=" + group
				+ " " + "dcTermsDate=" + dcTermsDate + " " + "tookPlaceAt=" + tookPlaceAt + " "
				+ "part=" + part + " " + "description=" + description + " "
				+ "collectionId=" + collectionId + " " + "collection=" + collection + " " + "extraNote=" + extraNote
				+ "]";

	}

}
