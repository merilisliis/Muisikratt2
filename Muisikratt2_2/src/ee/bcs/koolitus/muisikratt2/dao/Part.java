package ee.bcs.koolitus.muisikratt2.dao;

public class Part {
	private int partId;
	private String partName;

	public int getPartId() {
		return partId;
	}

	public Part setPartId(int partId) {
		this.partId = partId;
		return this;
	}

	public String getPartName() {
		return partName;
	}

	public Part setPartName(String partName) {
		this.partName = partName;
		return this;
	}

	@Override
	public String toString() {
		return "Part[partId=" + partId + "partName" + partName + "]";

	}
}
