package ee.bcs.koolitus.muisikratt2.main;

import java.util.List;

import ee.bcs.koolitus.muisikratt2.dao.MuisData;
import ee.bcs.koolitus.muisikratt2.source.MuisDataResource;

//Muide, main-meetodit ei ole veebiteenusel otseselt vaja! Seda ei käivitata pärast rets-teenuse käimasaamist. 
//ehk siis - süsteem on juhi nööri otsast vaba :-) 

public class Main {
	// See on kõigi andmete välja küsimiseks: READ
	//Tahame proovida, kas andmebaasist andmete küsimine toimib
	public static void main(String[] args) {
		List<MuisData> items;
		MuisDataResource resource = new MuisDataResource();
		items = resource.getAllItems();
		System.out.println(items);
	}
}
