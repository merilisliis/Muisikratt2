package ee.bcs.koolitus.muisikratt2.source;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;

import ee.bcs.koolitus.muisikratt2.source.DatabaseConnection;
import ee.bcs.koolitus.muisikratt2.dao.Collection;

public class CollectionResource {
	// Selle meetodiga küsime andmebaasist andmeid (READ)
	public static List<Collection> getAllCollections() {
		List<Collection> collections = new ArrayList<>();
		String sqlQuery = "SELECT * FROM collections";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				Collection collection = new Collection().setId(results.getInt("id"))
						.setCollectionName(results.getString("collection_name"));

				collections.add(collection);
			}

		} catch (SQLException e) {
			System.out.println("Error on getting collections set: " + e.getMessage());
		}

		return collections;
	}

	public static Collection getCollectionById(int id) {
		Collection collection = null;
		String sqlQuery = "SELECT * FROM collections WHERE id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, id);
			ResultSet results = statement.executeQuery();
			// Siin all on ette antud, kuidas andmeid esitada, mis andmebaasist tulevad
			while (results.next()) {
				collection = new Collection().setId(results.getInt("id"))
						.setCollectionName(results.getString("collection_name"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting collection set: " + e.getMessage());
		}

		return collection;
		
	}
		
}

