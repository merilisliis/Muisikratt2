package ee.bcs.koolitus.muisikratt2.source;

import java.lang.reflect.InvocationTargetException;
//minu java kood hakkab nüüd minu andmebaasi andmeid juurde panema ja andmeid lugema
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

//See on klass, millega loome ühenduse andmebaasiga 
public class DatabaseConnection {
	// Loome muutuja, mille kaudu ühenduma hakkame
	private static Connection connection = null;

	// Ehitame meetodi, mis hakkab ühendust looma
	public static Connection getConnection() {
		// Nüüd tuleb sisseehitatud meetod jdbc ja minu andmebaasi aadress
		String dbUrl = "jdbc:mysql://localhost:3306/muis";
		Properties connectionProperties = new Properties();
		// See alumine rida ütleb, et kes on praegu kasutajaks
		// (hea kui on tehtud rohkem kasutajaid, kellel on vähem õigusi.
		// Neid tehakse SQL-i poolel. Siin all on muutuja ja selle väärtuse
		// (kasutajanime ja parooli määramine)
		connectionProperties.put("user", "root");
		connectionProperties.put("password", "Assamalla200");
		loadDriver(); // - loaddriver meetodi väljakutse on siin

		try {
			connection = DriverManager.getConnection(dbUrl, connectionProperties);
		} catch (SQLException e) {
			System.out.println("Error on creating database connection: " + e.getMessage());
		}

		return connection;

	}

	// Nüüd laeme spetsiaalse draiveri ühenduse loomise jaoks
	private static void loadDriver() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException | ClassNotFoundException e) {
			System.out.println("Error on loading driver: " + e.getMessage());
		}
	}

	public static void closeConnection(Connection connection) {
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Error on closing database connection:" + e.getMessage());
		}
	}
}