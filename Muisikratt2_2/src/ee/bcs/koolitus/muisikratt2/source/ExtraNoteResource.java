package ee.bcs.koolitus.muisikratt2.source;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.muisikratt2.dao.ExtraNote;
import ee.bcs.koolitus.muisikratt2.source.DatabaseConnection;

public class ExtraNoteResource {
	// Selle meetodiga küsime andmebaasist andmeid (READ)

	public static ExtraNote getExtraNoteById(int idExtraNote) {
		ExtraNote extraNote = null;
		String sqlQuery = "SELECT * FROM extra_notes WHERE id_extra_note=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, idExtraNote);
			ResultSet results = statement.executeQuery();
			// Siin all on ette antud, kuidas andmeid esitada, mis andmebaasist tulevad
			while (results.next()) {
				extraNote = new ExtraNote().setIdExtraNote(results.getInt("id_extra_note"))
						.setMuisLink(results.getString("muis_link")).setDate(results.getTimestamp("date"))
						.setEMail(results.getString("e_mail")).setClientName(results.getString("client_name"))
						.setExtraNoteText(results.getString("extra_note_text"));
			}
		} catch (SQLException e) {
			System.out.println("Error on getting part set: " + e.getMessage());
		}

		return extraNote;

	}

	public static ExtraNote addExtraNote(ExtraNote extraNote) {
		String sqlQuery = "INSERT INTO extra_notes (muis_link, e_mail, client_name, extra_note_text) "
				+ "VALUES (?, ?, ?, ?)";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery,
				Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, extraNote.getMuisLink());
			statement.setString(2, extraNote.getEMail());
			statement.setString(3, extraNote.getClientName());
			statement.setString(4, extraNote.getExtraNoteText());
			System.out.println(statement);
			statement.executeUpdate();
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				extraNote.setIdExtraNote(resultSet.getInt(1));
			}
		} catch (SQLException e) {
			System.out.println("Error on adding extraNote: " + e.getMessage());
		}

		return extraNote;
	}
//Päring mySQL-ist kõigi märkuste saamiseks muis_link välja väärtuse võrdlemise järgi 
	public static List<ExtraNote> getExtraNotesByMuisLink(String muisLink) {
		List<ExtraNote> extraNotesList = new ArrayList<>();
		String sqlQuery = "SELECT * FROM extra_notes WHERE muis_link=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setString(1, muisLink);
			ResultSet results = statement.executeQuery();
			// Siin all on ette antud, kuidas andmeid esitada, mis andmebaasist tulevad
			while (results.next()) {
				ExtraNote extraNote = new ExtraNote().setIdExtraNote(results.getInt("id_extra_note"))
						.setMuisLink(results.getString("muis_link")).setDate(results.getTimestamp("date"))
						.setEMail(results.getString("e_mail")).setClientName(results.getString("client_name"))
						.setExtraNoteText(results.getString("extra_note_text"));
				extraNotesList.add(extraNote);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting part set: " + e.getMessage());
		}

		return extraNotesList;

	}
}