package ee.bcs.koolitus.muisikratt2.source;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.muisikratt2.dao.MuisData;
import ee.bcs.koolitus.muisikratt2.source.CollectionResource;
import ee.bcs.koolitus.muisikratt2.source.PartResource;
import ee.bcs.koolitus.muisikratt2.source.ExtraNoteResource;

public class MuisDataResource {
	// Selle meetodiga küsime andmebaasist andmeid (READ)
	public static List<MuisData> getAllItems() {
		List<MuisData> items = new ArrayList<>();
		String sqlQuery = "SELECT m.*,COUNT(en.id_extra_note) AS extra_notes_count FROM muis_data m LEFT JOIN extra_notes en ON m.muis_link=en.muis_link GROUP BY m.id_local";
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

			while (results.next()) {
				MuisData item = new MuisData().setIdLocal(results.getInt("id_local"))
						.setMuisLink(results.getString("muis_link")).setIdentifier(results.getString("identifier"))
						.setEssence(results.getString("essence")).setTitle(results.getString("title"))
						.setPerson(results.getString("person")).setGroup(results.getString("group"))
						.setDcTermsDate(results.getString("dcterms_date"))
						.setTookPlaceAt(results.getString("took_place_at"))
						.setPart(PartResource.getPartById(results.getInt("id_collection_forms_part_of")))
						.setDescription(results.getString("description"))
						.setCollection(CollectionResource.getCollectionById(results.getInt("collection_id")))
						.setExtraNote(ExtraNoteResource.getExtraNoteById(results.getInt("id_extra_note")))
						.setNumberOfNotes(results.getInt("extra_notes_count"));

				items.add(item);
			}

		} catch (SQLException e) {
			System.out.println("Error on getting items set: " + e.getMessage());
		}

		return items;
	}// siin all on meetod getItemById üksikmuseaalide andmete kätte saamiseks
		// idLocal-i kaudu.
		// Seda läheb vaja Märkuse lisamiseks kindla id-ga museaalile - et märkuse sisu
		// läheks ühe kindla id-ga eseme juurde.

	public static MuisData getItemById(int idLocal) {
		MuisData item = null;
		String sqlQuery = "SELECT * FROM muis_data WHERE id_local=" + idLocal;
		try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {
			// Siin all on ette antud, kuidas andmeid esitada, mis andmebaasist tulevad
			while (results.next()) {
				item = new MuisData().setIdLocal(results.getInt("id_local")).setMuisLink(results.getString("muis_link"))
						.setIdentifier(results.getString("identifier")).setEssence(results.getString("essence"))
						.setTitle(results.getString("title")).setPerson(results.getString("person"))
						.setGroup(results.getString("group")).setDcTermsDate(results.getString("dcterms_date"))
						.setTookPlaceAt(results.getString("took_place_at"))
						.setPart(PartResource.getPartById(results.getInt("id_collection_forms_part_of")))
						.setDescription(results.getString("description"))
						.setCollectionId(results.getInt("collection_id"))
						.setExtraNote(ExtraNoteResource.getExtraNoteById(results.getInt("id_extra_note")));

			}
		} catch (SQLException e) {
			System.out.println("Error on getting items set: " + e.getMessage());
		}

		return item;
	}

	// See meetod on märkuse lisamiseks mySQL-i väljale extra_note:
	public static MuisData addExtraNote(MuisData item) {
		String sqlQuery = "INSERT INTO muis_data (extra_note)" + "VALUES ('" + item.getExtraNote() + "')";
		try (Statement statement = DatabaseConnection.getConnection().createStatement()) {
			statement.executeUpdate(sqlQuery, Statement.RETURN_GENERATED_KEYS);
			ResultSet resultSet = statement.getGeneratedKeys();
			while (resultSet.next()) {
				item.setIdLocal(resultSet.getInt(1)); // võtme järjekorranr
			}
		} catch (SQLException e) {
			System.out.println("Error on adding extra note to an item: " + e.getMessage());
		}
		return item;
	}
	// Siin on meetod kollektsiooni nime järgi muis_data tabeli sisu kuvamiseks.

	public static List<MuisData> getItemsByCollectionId(int collectionId) {
		List<MuisData> items = new ArrayList<>();
		String sqlQuery = "SELECT * FROM muis_data WHERE collection_id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, collectionId);
			ResultSet results = statement.executeQuery();
			// Siin all on ette antud, kuidas andmeid esitada, mis andmebaasist tulevad
			while (results.next()) {
				MuisData item = new MuisData().setIdLocal(results.getInt("id_local"))
						.setMuisLink(results.getString("muis_link")).setIdentifier(results.getString("identifier"))
						.setEssence(results.getString("essence")).setTitle(results.getString("title"))
						.setPerson(results.getString("person")).setGroup(results.getString("group"))
						.setDcTermsDate(results.getString("dcterms_date"))
						.setTookPlaceAt(results.getString("took_place_at"))
						.setPart(PartResource.getPartById(results.getInt("id_collection_forms_part_of")))
						.setDescription(results.getString("description"))
						.setCollection(CollectionResource.getCollectionById(results.getInt("collection_id")))
						.setExtraNote(ExtraNoteResource.getExtraNoteById(results.getInt("id_extra_note")));
				items.add(item);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting items set with given collection id: " + e.getMessage());
		}

		return items;
	}

	public static List<MuisData> getItemsByPartId(int partId) {
		List<MuisData> items = new ArrayList<>();
		String sqlQuery = "SELECT * FROM muis_data WHERE id_collection_forms_part_of=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, partId);
			ResultSet results = statement.executeQuery();
			// Siin all on ette antud, kuidas andmeid esitada, mis andmebaasist tulevad
			while (results.next()) {
				MuisData item = new MuisData().setIdLocal(results.getInt("id_local"))
						.setMuisLink(results.getString("muis_link")).setIdentifier(results.getString("identifier"))
						.setEssence(results.getString("essence")).setTitle(results.getString("title"))
						.setPerson(results.getString("person")).setGroup(results.getString("group"))
						.setDcTermsDate(results.getString("dcterms_date"))
						.setTookPlaceAt(results.getString("took_place_at"))
						.setPart(PartResource.getPartById(results.getInt("id_collection_forms_part_of")))
						.setDescription(results.getString("description"))
						.setCollection(CollectionResource.getCollectionById(results.getInt("collection_id")))
						.setExtraNote(ExtraNoteResource.getExtraNoteById(results.getInt("id_extra_note")));
				items.add(item);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting items set with given collection id: " + e.getMessage());
		}

		return items;
	}

	// Päring kahele tingimusele vastavate tulemuste saamiseks:
	public static List<MuisData> getItemsByCollectionIdAndPartId(int collectionId, int partId) {
		List<MuisData> items = new ArrayList<>();
		String sqlQuery = "SELECT * FROM muis_data WHERE id_collection_forms_part_of=? AND collection_id=?";
		try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
			statement.setInt(1, partId);
			statement.setInt(2, collectionId);
			ResultSet results = statement.executeQuery();
			while (results.next()) {
				MuisData item = new MuisData().setIdLocal(results.getInt("id_local"))
						.setMuisLink(results.getString("muis_link")).setIdentifier(results.getString("identifier"))
						.setEssence(results.getString("essence")).setTitle(results.getString("title"))
						.setPerson(results.getString("person")).setGroup(results.getString("group"))
						.setDcTermsDate(results.getString("dcterms_date"))
						.setTookPlaceAt(results.getString("took_place_at"))
						.setPart(PartResource.getPartById(results.getInt("id_collection_forms_part_of")))
						.setDescription(results.getString("description"))
						.setCollection(CollectionResource.getCollectionById(results.getInt("collection_id")))
						.setExtraNote(ExtraNoteResource.getExtraNoteById(results.getInt("id_extra_note")));
				items.add(item);
			}
		} catch (SQLException e) {
			System.out.println("Error on getting items set with given collection id: " + e.getMessage());
		}

		return items;
	}
}
