package ee.bcs.koolitus.muisikratt2.source;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.koolitus.muisikratt2.dao.Part;
import ee.bcs.koolitus.muisikratt2.source.DatabaseConnection;

public class PartResource {
	// Selle meetodiga küsime andmebaasist andmeid (READ)
		public static List<Part> getAllParts() {
			List<Part> parts = new ArrayList<>();
			String sqlQuery = "SELECT * FROM parts";
			try (ResultSet results = DatabaseConnection.getConnection().createStatement().executeQuery(sqlQuery);) {

				while (results.next()) {
					Part part = new Part().setPartId(results.getInt("id_collection_forms_part_of"))
							.setPartName(results.getString("part_name"));

					parts.add(part);
				}

			} catch (SQLException e) {
				System.out.println("Error on getting part set: " + e.getMessage());
			}

			return parts;
		}

		public static Part getPartById(int partId) {
			Part part = null;
			String sqlQuery = "SELECT * FROM parts WHERE id_collection_forms_part_of=?";
			try (PreparedStatement statement = DatabaseConnection.getConnection().prepareStatement(sqlQuery)) {
				statement.setInt(1, partId);
				ResultSet results = statement.executeQuery();
				// Siin all on ette antud, kuidas andmeid esitada, mis andmebaasist tulevad
				while (results.next()) {
					part = new Part().setPartId(results.getInt("id_collection_forms_part_of"))
							.setPartName(results.getString("part_name"));
				}
			} catch (SQLException e) {
				System.out.println("Error on getting part set: " + e.getMessage());
			}

			return part;
			
		}

}
