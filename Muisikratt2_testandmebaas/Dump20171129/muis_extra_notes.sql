CREATE DATABASE  IF NOT EXISTS `muis` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `muis`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: muis
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `extra_notes`
--

DROP TABLE IF EXISTS `extra_notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `extra_notes` (
  `id_extra_note` int(11) NOT NULL AUTO_INCREMENT,
  `muis_link` varchar(100) DEFAULT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `e_mail` varchar(100) DEFAULT NULL,
  `client_name` varchar(45) DEFAULT NULL,
  `extra_note_text` text,
  PRIMARY KEY (`id_extra_note`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `extra_notes`
--

LOCK TABLES `extra_notes` WRITE;
/*!40000 ALTER TABLE `extra_notes` DISABLE KEYS */;
INSERT INTO `extra_notes` VALUES (1,'ddsfgfg','2017-11-28 11:17:10','jhfkgjhdfj','fgdfgd','ffgdfgs'),(2,'http://opendata.muis.ee/object/350951','2017-11-28 11:21:08',NULL,'jhgkdfg','fsdgjd k gkjdf sölgkj dsfgöl gj gfjdöl'),(3,'http://opendata.muis.ee/object/350951','2017-11-28 14:25:31',NULL,'kljhkgjidgf','kfhjhgchgc'),(4,'http://opendata.muis.ee/object/350951','2017-11-28 14:32:48',NULL,'yrfkuyfuyf','kuttcutc'),(5,'http://opendata.muis.ee/object/350951','2017-11-28 14:35:18',NULL,'jydfthd','kjuttyrzju'),(6,'http://opendata.muis.ee/object/350951','2017-11-28 14:44:42',NULL,'jyhtd','kutjyurtzyjr'),(7,'http://opendata.muis.ee/object/350951','2017-11-28 14:52:27',NULL,'jfgxjuyx','kutxkutx'),(8,'http://opendata.muis.ee/object/350951','2017-11-28 14:56:54',NULL,'dfgjdfsgjdfgjk','dstgfjdfgkjcdgfhk'),(9,'http://opendata.muis.ee/object/350951','2017-11-28 15:00:59',NULL,'jhgkdfg','fsdgjd k gkjdf sölgkj dsfgöl gj gfjdöl'),(10,'http://opendata.muis.ee/object/350951','2017-11-28 15:05:47',NULL,'fdsgdfs','fdgdfsg'),(11,'http://opendata.muis.ee/object/350951','2017-11-28 15:06:23',NULL,'jhgkdfg','fsdgjd k gkjdf sölgkj dsfgöl gj gfjdöl'),(12,'http://opendata.muis.ee/object/350951','2017-11-28 15:06:59','jfhkdjshfkjsha','jhgkdfg','fsdgjd k gkjdf sölgkj dsfgöl gj gfjdöl'),(13,'http://opendata.muis.ee/object/350951','2017-11-28 15:07:28','fdgdf','dfgdfs','dfgdfsg'),(14,'http://opendata.muis.ee/object/348360','2017-11-28 15:11:23','gdgds','sdfgdsfg','dsfgdsgfsd'),(15,'http://opendata.muis.ee/object/350951','2017-11-28 15:15:28','dfgsd','fgsdg','fsgdsf'),(16,'http://opendata.muis.ee/object/350951','2017-11-28 15:17:09','fsdf','sdfgfd','sdfgdf'),(17,'http://opendata.muis.ee/object/350951','2017-11-28 15:17:11','fsdf','sdfgfd','sdfgdf'),(18,'http://opendata.muis.ee/object/350951','2017-11-28 15:17:25','fsdf','sdfgfd','sdfgdfghgfh'),(19,'http://opendata.muis.ee/object/350951','2017-11-28 15:18:59','ghdf','hdfh','dfghdf'),(20,'http://opendata.muis.ee/object/348360','2017-11-28 15:20:21','sf','fgd','fgdfg'),(21,'http://opendata.muis.ee/object/350951','2017-11-28 16:12:01','','fgd','fgdfg'),(22,'http://opendata.muis.ee/object/350951','2017-11-28 16:26:19','gfdyhkdftgyk','ghkf','fghkfghk'),(23,'http://opendata.muis.ee/object/350951','2017-11-28 16:33:44','1dfghrfth','dfg','adfsgh'),(24,'http://opendata.muis.ee/object/350951','2017-11-28 16:39:17','1dfghrfth','dfg','adfsgh'),(25,'http://opendata.muis.ee/object/350951','2017-11-29 09:47:41','hghgchg','gjfhgdhgf','jhfhgchgc'),(26,'http://opendata.muis.ee/object/350951','2017-11-29 10:41:53','dgfhsf','fsghnsfg','sfghnsfgjh'),(27,'http://opendata.muis.ee/object/1475882','2017-11-29 10:56:58','dfasf','dfasda','fsasdf'),(28,'http://opendata.muis.ee/object/3007431','2017-11-29 10:57:44','fsfg','dgds','sdfgdsg'),(29,'http://opendata.muis.ee/object/350951','2017-11-29 11:40:41','bv b ','hfjfhgv','jhvgcvkhgc');
/*!40000 ALTER TABLE `extra_notes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-29 11:54:27
