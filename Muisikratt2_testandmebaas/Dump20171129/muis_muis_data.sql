CREATE DATABASE  IF NOT EXISTS `muis` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `muis`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: muis
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `muis_data`
--

DROP TABLE IF EXISTS `muis_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `muis_data` (
  `id_local` int(11) NOT NULL AUTO_INCREMENT,
  `muis_link` varchar(100) DEFAULT NULL,
  `identifier` varchar(45) NOT NULL,
  `essence` varchar(45) DEFAULT NULL,
  `title` text,
  `person` varchar(100) DEFAULT NULL,
  `group` varchar(100) DEFAULT NULL,
  `dcterms_date` varchar(100) DEFAULT NULL,
  `took_place_at` varchar(100) DEFAULT NULL,
  `id_collection_forms_part_of` int(11) DEFAULT NULL,
  `description` text,
  `collection_id` int(11) DEFAULT NULL,
  `id_extra_note` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_local`),
  UNIQUE KEY `kogunumber_UNIQUE` (`identifier`),
  KEY `collection_id_idx` (`collection_id`),
  CONSTRAINT `FK_collection_id` FOREIGN KEY (`collection_id`) REFERENCES `collections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `muis_data`
--

LOCK TABLES `muis_data` WRITE;
/*!40000 ALTER TABLE `muis_data` DISABLE KEYS */;
INSERT INTO `muis_data` VALUES (1,'http://opendata.muis.ee/object/350951','HMK _ 717 AJ 266','	konks; saapad','Saapanööpide kinnitamise konks',NULL,NULL,'	arvatav aeg .1930 - 1940','Eesti Harju',NULL,NULL,1,8),(2,'http://opendata.muis.ee/object/348360','HMK _ 47 AJ 21','	tahvel','	koolitahvel',NULL,NULL,'	arvatav aeg - 1940','	Eesti',3,'Musta värvi kiltkivist tahvel, puust raam. Tahvlikivi on murdunud, murdunud osa ei ole. Raami ülemises osas pealmisel küljel tekst. ',1,7),(3,'http://opendata.muis.ee/object/3007431','	HMK _ 8890 AJ 3275','	pits (tekstiil)','	pits, Pakri',NULL,NULL,'enne .1940','Eesti; saar Pakri',NULL,NULL,1,5),(4,'http://opendata.muis.ee/object/1475882','HMK _ 31 AR 24','dokument','	Keila Vabatahtliku Tuletõrjeühingu liikmete avaldused ja teadaanded ühingusse astumiseks',NULL,NULL,'1913 - 1941',NULL,NULL,NULL,2,NULL),(5,'http://opendata.muis.ee/object/1532759','HMK _ 1144 AR 709','silt/märk','	Puhkus ja Elurõõm Lääne-Harju suvemängud jaanipäeval 1942 Keilas',NULL,NULL,'1942',NULL,NULL,NULL,2,NULL),(6,'http://opendata.muis.ee/object/1475896','HMK _ 33 AR 31','raamat','	Protokolliraamat. Valingu, Jõgisoo, Koppelmanni ja Voore Kooli Kohtuprotokolli raamat 1875-31.01.1905. Lisada Vanamõisa al. l. 43',NULL,NULL,'	12.11.1875 - 31.1.1905',NULL,NULL,NULL,2,NULL),(7,'http://opendata.muis.ee/object/347110','HMK _ 4879 AJ 1377','medal','medal: juubelilaulupeoks 1869-1969',NULL,NULL,'1969',NULL,3,NULL,1,NULL),(8,'http://opendata.muis.ee/object/348736','HMK _ 2419 AJ 717','raha','münt, Saksa Riigi 5 penni 1874.a.',NULL,NULL,'1874',NULL,NULL,NULL,1,NULL),(9,'http://opendata.muis.ee/object/1546831','HMK _ 6537 AR 4215',NULL,'Sauna tunnistus nr 253 Loksa koolilt tõendusega, et Rudolf Oja on saunasõprade seltsi liige. 19. dets. 1969',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL),(10,'http://opendata.muis.ee/object/3062532','HMK _ 8974 AR 5354','kavand/joonis/eskiis','Elamu juurdeehituse kavand',NULL,NULL,'1935',NULL,NULL,NULL,2,NULL),(11,'http://opendata.muis.ee/object/348110','HMK _ 3631 AR 2512','raamat','Taani Hindamisraamat Harjumaa vanimatest küladest',NULL,NULL,NULL,NULL,NULL,NULL,2,NULL),(12,'http://opendata.muis.ee/object/3066263','HMK _ 9119 A 2407','arheoloogiline leid','Püssikatke',NULL,NULL,'arvatav aeg .1380 - 1400',NULL,NULL,NULL,3,NULL),(13,'http://opendata.muis.ee/object/348549','HMK _ 1155:42 A 42','arheoloogiline leid','Ammunoole ots',NULL,NULL,NULL,NULL,1,NULL,3,NULL),(14,'http://opendata.muis.ee/object/1121623','HMK _ 5384:58 A 687','münt','Münt, Riia penn',NULL,NULL,'1479 - 1484',NULL,NULL,NULL,3,NULL),(15,'http://opendata.muis.ee/object/348746','HMK _ 7111 AV 12','andmekandja','	VHS kassett Volbripäev, Suvistepüha',NULL,NULL,NULL,NULL,NULL,NULL,4,NULL),(16,'http://opendata.muis.ee/object/351407','HMK _ 8302 AV 21','helisalvestis','CD Keila kodukandi lood',NULL,NULL,NULL,NULL,NULL,NULL,4,NULL),(17,'http://opendata.muis.ee/object/348466','	HMK _ 6334 AV 3','videosalvestis','Videokassett Tulge külla Kehra',NULL,NULL,'1999','Eesti Kehra',NULL,'saadete sari \"Tulge külla\" TV3s, eetris 7.02.1999',4,NULL),(18,'http://opendata.muis.ee/object/3234729','	HMK _ F 7100','	foto','	Leeripilt, Keila',NULL,NULL,'	1900 - 1920','Eesti Keila',NULL,'Marie Tamm (Varma) leeripilt',5,NULL),(19,'http://opendata.muis.ee/object/1664960','HMK _ F 6956','postkaart','Harju-Madise kirik','Siim, Joel',NULL,'2011','Eesti Madise',NULL,NULL,5,NULL),(20,'http://opendata.muis.ee/object/2023479','HMK _ F 3360','foto','Nõukogude armee lahkumine Keila raudteejaamas',NULL,NULL,'1994',NULL,NULL,NULL,5,NULL),(21,'http://opendata.muis.ee/object/351417','HMK _ 7139 G 12','kivim','	Oksüdeerunud \"põhja punase\" kihipind',NULL,NULL,NULL,NULL,NULL,NULL,6,NULL),(22,'http://opendata.muis.ee/object/351390','HMK _ 7142:2 G 17','kivim','Tuula \"marmor\"',NULL,NULL,NULL,NULL,NULL,NULL,6,NULL),(23,'http://opendata.muis.ee/object/351408','HMK _ 5999 G 4','kivim','Püriit',NULL,NULL,NULL,NULL,2,NULL,6,NULL),(24,'http://opendata.muis.ee/object/350236','HMK _ 2105 K 69','joonistus','M. Lermantovi portree','Hermann, Karl August','','7.8.1948 - 7.8.1948',NULL,NULL,NULL,7,NULL),(25,'http://opendata.muis.ee/object/1384798','HMK _ 8805 K 222','maal','Paldiski sadam','	Adamson, Amandus Heinrich',NULL,'1898',NULL,3,NULL,7,NULL),(26,'http://opendata.muis.ee/object/1667575','HMK _ 8830 K 225','skulptuur','Alfa et omega','	Adamson, Amandus Heinrich',NULL,'1923 ',NULL,3,NULL,7,NULL);
/*!40000 ALTER TABLE `muis_data` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-29 11:54:27
